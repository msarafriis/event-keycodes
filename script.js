const insert = document.getElementById('insert')
const eventElements = [
  document.createElement('div'),
  document.createElement('div'),
  document.createElement('div')
]
const eventTextNodes = [
  document.createTextNode(''),
  document.createTextNode(''),
  document.createTextNode('')
]
eventElements.forEach((element, index) => {
  element.className = 'key'
  element.appendChild(eventTextNodes[index])

  let labelText
  switch (index) {
    case 0:
      labelText = document.createTextNode('event.key')
      break
    case 1:
      labelText = document.createTextNode('event.keyCode')
      break
    case 2:
      labelText = document.createTextNode('event.code')
      break
    default:
      console.error('Invalid Element')
  }
  const label = document.createElement('small')
  label.appendChild(labelText)
  element.appendChild(label)
})

window.addEventListener('keydown', (e) => {
  const elements = initialize()

  elements['event.key'].textContent = e.key === ' ' ? 'Space' : e.key
  elements['event.keyCode'].textContent = e.keyCode
  elements['event.code'].textContent = e.code
})

function initialize() {
  if (insert.childElementCount < 3) {
    while (insert.lastChild) {
      insert.removeChild(insert.lastChild)
    }
    eventElements.forEach((element) => {
      insert.appendChild(element)
    })
  }

  return {
    'event.key': eventElements[0].firstChild,
    'event.keyCode': eventElements[1].firstChild,
    'event.code': eventElements[2].firstChild
  }
}

// Original Code
// window.addEventListener('keydown', (e) => {
//   insert.innerHTML = `
//       <div class="key">${
//         e.key === ' ' ? 'Space' : e.key
//       }<small>event.key</small></div>
//       <div class="key">${e.keyCode}<small>event.keyCode</small></div>
//       <div class="key">${e.code}<small>event.code</small></div>
//       `
// })
