# Event KeyCodes

Getting keycodes with JavaScript. Nice. Might be actually pretty useful.

I opted to modify it to use the dynamic DOM instead after doing the way shown.

This is part of a course by [Brad Traversy and Florin Pop](https://github.com/bradtraversy/50projects50days).
